#include <iostream>
using namespace std;

int main() {
    int kampioensjarenFeyenoord[] = {1924, 1928, 1936, 1938, 1940, 1961, 1962, 1965, 1969, 1971, 1974, 1984, 1993, 1999};

    auto eerste = kampioensjarenFeyenoord[0];

    cout << "Feyenoord was voor het eerst landskampioen in: " << eerste;

    cin.get();
    return 0;
}