/* Een terugblik op functie, struct en array */

#include <stdio.h> 
/* nodig voor gebruik printf (schrijven naar scherm) 
                   en scanf  (lezen uit toetsenbord) */
 
typedef struct { /* Een Tijdsduur bestaat uit:  */
    int uur;     /*    een aantal uren en       */
    int minuten; /*    een aantal minuten.      */
} Tijdsduur;

/* Deze functie drukt een Tijdsduur af */
void drukaf(Tijdsduur td) {
    if (td.uur == 0)
        printf("           %2d minuten\n", td.minuten);
    else
        printf("%3d uur en %2d minuten\n", td.uur, td.minuten);
}

/* Deze functie drukt een rij met een aantal gewerkte tijden af */
void drukafRij(Tijdsduur rij[], int aantal) {
    int teller;
    for (teller = 0; teller < aantal; teller++)
        drukaf(rij[teller]);
}

/* Deze functie berekent de totaal gewerkte tijd uit een rij met een aantal gewerkte tijden */
Tijdsduur som(Tijdsduur rij[], int aantal) {
    int teller;
    Tijdsduur s = {0, 0};
    for (teller = 0; teller < aantal; teller++) {
        s.uur += rij[teller].uur;
        s.minuten += rij[teller].minuten;
    }
    s.uur += s.minuten / 60;
    s.minuten %= 60;
    return s;
}

#define MAX 5

int main(void) {
    Tijdsduur gewerkteTijdenRij[MAX];
    int aantal = 0, gelezen;
    do {
        printf("Type gewerkte uren en minuten in (of Ctrl-Z): ");
        gelezen = scanf("%d%d", &gewerkteTijdenRij[aantal].uur, &gewerkteTijdenRij[aantal].minuten);
    }
    while (gelezen == 2 && ++aantal < MAX);
    printf("\n\n");
    drukafRij(gewerkteTijdenRij, aantal);
    printf("De totaal gewerkte tijd is:\n");
    drukaf(som(gewerkteTijdenRij, aantal));
    fflush(stdin); getchar();
    return 0;
}
