# Dictaat Objectgeoriënteerd Programmeren in C++ #

In dit repository is het dictaat Objectgeoriënteerd Programmeren in C++ opgenomen. 
Je kunt de gecompileerde versies (in pdf) vinden in de [Downloads](https://bitbucket.org/HarryBroeders/dictaat-objectgeori-nteerd-programmeren-in-c/downloads) van dit repository.

Op- en aanmerkingen zijn altijd welkom. Maak een [issue](https://bitbucket.org/HarryBroeders/dictaat-objectgeori-nteerd-programmeren-in-c/issues?status=new&status=open) aan of stuur een mail naar [Harry Broeders](mailto:harry@hc11.demon.nl).