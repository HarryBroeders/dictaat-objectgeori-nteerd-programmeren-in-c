# Define target and source file here:
TARGET       = Dictaat_OGPiCpp
SOURCE       = dictaat
DEPENDENCIES = style.tex styleCpp.tex

# Define font here:
#FONT         = \timestrue
FONT         = \chartertrue
# For standard latex fonts use:
#FONT         = 

# Options:
PDFLATEXOPT  = -shell-escape -interaction=nonstopmode -file-line-error

LATEXBINDIR  = C:/texlive/2018/bin/win32
UTILSBINDIR  = C:/gnuwin32/bin
PDFLATEX     = $(LATEXBINDIR)\pdflatex.exe
BIBER        = $(LATEXBINDIR)\biber.exe
RM           = $(UTILSBINDIR)\rm.exe
MAKE         = $(UTILSBINDIR)\make.exe
MV           = $(UTILSBINDIR)\mv.exe

.PHONY : all
all : $(TARGET).pdf $(TARGET)_ebook.pdf

.PHONY : clean
clean :
	$(RM) -f $(SOURCE).log $(SOURCE).toc $(SOURCE).out $(SOURCE).bcf $(SOURCE).aux $(SOURCE).blg $(SOURCE).bbl $(SOURCE).run.xml $(SOURCE).synctex.gz

.PHONY : cleanall
cleanall :
	$(MAKE) clean
	$(RM) -f $(TARGET).pdf $(TARGET)_ebook.pdf

.PHONY : build
build :
	$(MAKE) cleanall
	$(MAKE)

$(TARGET)_ebook.pdf : $(SOURCE).tex $(DEPENDENCIES)
	$(RM) -f args.tex
	echo '\ebooktrue' >args.tex
	echo '$(FONT)' >>args.tex
	$(PDFLATEX) $(SOURCE).tex
	$(BIBER) $(SOURCE)
	$(PDFLATEX) $(SOURCE).tex
	$(MV) $(SOURCE).pdf $(TARGET)_ebook.pdf

$(TARGET).pdf : $(SOURCE).tex $(DEPENDENCIES)
	$(RM) -f args.tex
	echo '\ebookfalse' >args.tex
	echo '$(FONT)' >>args.tex
	$(PDFLATEX) $(SOURCE).tex
	$(BIBER) $(SOURCE)
	$(PDFLATEX) $(SOURCE).tex
	$(MV) $(SOURCE).pdf $(TARGET).pdf
